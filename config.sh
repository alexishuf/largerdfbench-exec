#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage() {
  echo "Usage: $0 PARAM [DATASET_NAME]"
  echo ""
  echo "Where PARAM is one of:"
  echo "    list_ds: List all dataset names, separeted by spaces, in one line"
  echo "    list_bsbm_ds: List all BSBM dataset names, separeted by spaces, in one line"
  echo "    get_ep: Get the SPARQL endpoint URI given an dataset name"
  echo "    list_eps: List all URIs of all SPARQL endpoints"
  echo "    ext: Extension (7z or zip) of the Google drive file"
  echo "    gs_id: Google drive file ID of the dump"
  echo "    virtuoso_gs_id: Google drive file ID of the Virtuoso endpoint archive"
  echo "    port: HTTP port where the SPARQL endpoint should listen" 
}

function clean_dataset() {
  NAME=$(echo $1 | sed -E 's@(^.*/)?([^./]+(\.[^.]*)$)@\2@')
  if [ -z "$NAME" ]; then
    echo "Bad dataset name: $1" 1>&2
    exit 1
  fi
  echo $NAME
}
  
function list_ds() {
  echo DBPedia-Subset KEGG SWDFood NYT LinkedTCGA-E LinkedTCGA-A LinkedTCGA-M GeoNames DrugBank ChEBI Jamendo Affymetrix LMDB
}

function list_bsbm_ds() {
  echo Product ProductType ProductFeature Producer Vendor Offer Person Review
}

function get_ext() {
  ( echo $1 | grep LinkedTCGA &>/dev/null ) && echo 7z || echo zip
}

function get_gd_id() {
  DS=$(clean_dataset $1)
  case $1 in
    DBPedia-Subset)
      echo 0B1tUDhWNTjO-QWk5MVJud3cxUXM
      ;;
    KEGG)
      echo 0B1tUDhWNTjO-TUdUcllRMGVJaHM
      ;;
    SWDFood)
      echo 0B1tUDhWNTjO-RjBWZXYyX2FDT1E
      ;;
    NYT)
      echo 0B1tUDhWNTjO-dThoTm9DSmY4Wms
      ;;
    LinkedTCGA-E)
      echo 0B_MUFqryVpByOGdJWU8xNXR0aUE
      ;;
    LinkedTCGA-A)
      echo 0B_MUFqryVpByd2FVQ2gzOXhIemc
      ;;
    LinkedTCGA-M)
      echo 0B_MUFqryVpByQ0J2NFAtNVlzMUk
      ;;
    GeoNames)
      echo 0B1tUDhWNTjO-WEZZb2VwOG5vZkU
      ;;
    DrugBank)
      echo 0B1tUDhWNTjO-cVp5QV9VUWRuYkk
      ;;
    ChEBI)
      echo 0B1tUDhWNTjO-Vk81dGVkNVNuY1E
      ;;
    Jamendo)
      echo 0B1tUDhWNTjO-cWpmMWxxQ3Z2eVk
      ;;
    Affymetrix)
      echo 0B1tUDhWNTjO-eHVlZ1RyVVFJQU0
      ;;
    LMDB)
      echo 0B1tUDhWNTjO-bU5VN25NLXZXU0U
      ;;
    *)
      echo "Bad dataset name: $1" 1>&2
      exit 1
      ;;
  esac
}

function get_virtuoso_gd_id() {
  DS=$(clean_dataset $1)
  case $1 in
    DBPedia-Subset)
      echo 0B1tUDhWNTjO-OEgyXzBUVmlMQlk
      ;;
    KEGG)
      echo 0B1tUDhWNTjO-R1dKbDlHNXZ6blk
      ;;
    SWDFood)
      echo 0B1tUDhWNTjO-UW5HaF9rekdialU
      ;;
    NYT)
      echo 0B1tUDhWNTjO-RG9GeVdxbDR4YjQ
      ;;
    LinkedTCGA-E)
      echo 0B1tUDhWNTjO-RE5FaHp1eXFSenM
      ;;
    LinkedTCGA-A)
      echo 0B_MUFqryVpByYXdtbGhNYjdCZ3M
      ;;
    LinkedTCGA-M)
      echo 0B1tUDhWNTjO-ako3bHhwY0lVTm8
      ;;
    GeoNames)
      echo 0B_MUFqryVpByd3hJcHBPeHZhejA
      ;;
    DrugBank)
      echo 0B1tUDhWNTjO-U0V5Y0xDWXhzam8
      ;;
    ChEBI)
      echo 0B1tUDhWNTjO-Wk5LeHBzMUd3VHc
      ;;
    Jamendo)
      echo 0B1tUDhWNTjO-V3JMZjdfRkZxLUU
      ;;
    Affymetrix)
      echo 0B1tUDhWNTjO-Tm9oazNUdV9Cb1k
      ;;
    LMDB)
      echo 0B1tUDhWNTjO-NjVTVERvajJUcGc
      ;;
    *)
      echo "Bad dataset name: $1" 1>&2
      exit 1
      ;;
  esac
}


function get_port() {
  DS=$(clean_dataset $1)
  case $1 in
    DBPedia-Subset)
      echo 8891
      ;;
    KEGG)
      echo 8895
      ;;
    SWDFood)
      echo 8898
      ;;
    NYT)
      echo 8897
      ;;
    LinkedTCGA-E)
      echo 8888
      ;;
    LinkedTCGA-A)
      echo 8889
      ;;
    LinkedTCGA-M)
      echo 8887
      ;;
    GeoNames)
      echo 8893
      ;;
    DrugBank)
      echo 8892
      ;;
    ChEBI)
      echo 8890
      ;;
    Jamendo)
      echo 8894
      ;;
    Affymetrix)
      echo 8899
      ;;
    LMDB)
      echo 8896
      ;;
    Product)
      echo 8901
      ;;
    ProductType)
      echo 8902
      ;;
    ProductFeature)
      echo 8903
      ;;
    Producer)
      echo 8904
      ;;
    Vendor)
      echo 8905
      ;;
    Offer)
      echo 8906
      ;;
    Person)
      echo 8907
      ;;
    Review)
      echo 8908
      ;;
    *)
      echo "Bad dataset name: $1" 1>&2
      exit 1
      ;;
  esac
}

function get_ep() {
  DS=$(clean_dataset $1)
  if [ ! -f "$DIR/eps" ]; then
    echo "Missing $DIR/eps file"
    exit 1
  fi
  URI="$(sed -nE "s/^[ \t]*${DS}[ \t]*:[ \t]*([^ \t]*)[ \t]*$/\1/p" "$DIR/eps")"
  if [ -z "$URI" ]; then
    echo "No URI for dataset $DS"
    exit 1
  fi
  echo "$URI"
}

function priv_list_eps() {
  export IFS=$'\n'
  for ds in $(cat "$DIR/eps"); do
    echo $ds | sed -E 's/^[^:]+ *: *(http.*)$/\1/g'
  done
}

function list_eps() {
  ( priv_list_eps )
}

if [ -z "$1" ]; then
  usage 1>&2
  exit 1
fi

case $1 in
  list_ds)
    list_ds
    exit 0
    ;;
  list_bsbm_ds)
    list_bsbm_ds
    exit 0
    ;;
  get_ep)
    get_ep "$2"
    exit 0
    ;;
  list_eps)
    list_eps
    exit 0
    ;;
  ext)
    get_ext "$2"
    exit 0
    ;;
  gd_id)
    get_gd_id "$2"
    exit 0
    ;;
  virtuoso_gd_id)
    get_virtuoso_gd_id "$2"
    exit 0
    ;;
  port)
    get_port "$2"
    exit 0
    ;;
  *)
    echo "Bad param $1" 1>&2
    usage 1>&2
    ;;
esac
  
