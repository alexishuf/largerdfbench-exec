LargeRDFBench-exec
==================

This repository contains utilities to execute LargeRdfBench with riefederator and other mediators.

The goal is to automate:

- Virtuoso launch and memory usage configurations
- Load of dumps into TDB/TDB2 and starting Fuseki endpoints
- Configuration of the mediator systems (e.g., endpoint lists)
- Patching evaluation code to gather more performance metrics
- Patching datasets dumps to make them RDF 1.1 compliant (fixes unescaped URIs)

**Caveats/TODO**:

- Only FedX has received the patches and the driver script to run the experiments.

Running
-------

Simply type `./run.sh -h` to get the help. Subcommands and sub-subcomands also take -h as argument (e.g., `./run.sh bench fedx -h`).

```
Usage: ./run.sh [-h] [-c CHECKOUT_DIR] COMMAND COMMAND_ARGS...

Options:
    -h Show this help message\n
    -c CHECKOUT_DIR
       Checkout the LargeRDFBench-exec repo to the given CHECKOUT_DIR

Commands:
    fuseki       Start a fuseki endpoint for a patched dataset dump
    virtuoso     Start a virtuoso from the packaged endpoints
    bench        Run LargeRDFBench
```

`run.sh` is able to checkout the whole repositiory, so you can upload only run.sh to a remote machine. When you execute `run.sh` on that machine it will automatically do a shallow clone of the whole repository. This avoids suffering from slow domestic upload speeds to cloud hosts.

### Examples

Start a Fuseki server with 400 MB of heap serving the SWDFood dataset from a TDB2 on `/tmp/tdb.tmp`:

```
./run.sh fuseki -x 400M -4 -F /tmp -l /tmp/tdb.tmp SWDFood
```

Run FedX on all queries, with a hot cache. Each query runs 5 times, only the last three executions have the performance metrics stored to `results/bench-fedx.csv`:
```
rm -fr results/bench-fedx*  # optional: remove old results
./run.sh bench fedx -r 3 -p 2 
```

Running the whole experiment on AWS
-----------------------------------

**Warning**: this will cost money. Recomended setup are 13 t3a.large machines for the 13 datasets plus a t3a.medium unlimited for the mediators under evaluation. The dataset machines are large due to Virtuoso. For most datasets you could either run multiple Fuseki instances on a single machine or use lower tier machines.

**Warning**: Virtuoso really likes libssl.so.1.0.0/libcrypto.so.1.0.0 and other shared libraries which are not provided by any package in Amazon's AMI. The solution is not as trivial as creating the mentioned symlink. Experiments where tested on an arch linux AMI, but an AMI for a debian-derived distribution should also work.

Automation of the 13 virtuoso endpoints on AWS can be automated using `arch-vms/datasets.sh`:

```
Usage: ./datasets.sh [-h] COMMAND ...

Options:
    -h Show this help message\n
Commands:
    virtuoso  Start virtuoso in all 13 VMs (and creates/starts them, 
              if needed). LargeRDFBench-exec/eps will be updated.
    shutdown  Shutdown all 13 VMs
    terminate Shutdown and **TERMINATE** all 13 VMs
```

First, install `aws` CLI utils, then copy `secrets/aws-env.sh.tpl` to `secrets/aws-env.sh` and fill in your data. `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` can be omitted if you prefer to configure the `aws` CLI globally. After sourcing the `aws-env.sh` script your shell will change to alert you that AWS environment variables are set.

```
[you@host current-dir]$ source secrets/aws-env.sh
(aws keys) [you@host current-dir]$ 
```

### Experiment Walktrough

Manually prepare the mediator VM

1. Creata a VM on AWS to act as the mediator
2. SSH into this machine and checkout this repository
3. Configure secrets/aws-env.sh

From within the mediator VM shell, provision and test all VMs

1. Create virtuoso VMs with `arch-vms/datasets.sh provision`
2. Download dumps and set up Virtuoso on all machines: ``arch-vms/datasets.sh virtuoso``
3. Virtuoso is started in parallel on all machines and there is no barrier for waiting all 13 VMs. If this is the first time, downloads will be made and that may take a while. Wait at least 10m before continuing
5. Test the setup with `arch-vms/up-and-run.sh -U -S -q S3`. This will test only the simplest query. Index building may take a minute to complete. The -S and -U options disable bringing the VMs up and down.
6. Shutdown the machines to stop billing: `arch-vms/datasets.sh shutdown`

Once the VMs have been provisioned for the first time, only `arch-vms/up-and-run.sh` is necessary:

- Run all queries with FedX and riefederator: 
  `arch-vms/up-and-run.sh -f`
- Run all S* queries with riefederator only, taking only one measurement per query: 
  `arch-vms/up-and-run.sh -r 1 -g S`
