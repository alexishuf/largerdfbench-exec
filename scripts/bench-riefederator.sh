#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
RESULTS="$DIR/results"
ALL_ARGS=( "$0" "$@" )
RIEF_XMX=2048M
RIEF_DIR="$DIR/nobck/riefederator/"
RIEF_COM=master
RIEF_CHILD=0
RIEF_TOUT_MINS=10
OTR=0
RECORD_SINGLE=0
RUNS=1
PREHEAT_RUNS=0
ONLYPLAN=0

usage() {
  echo -e "Usage: $0 [-h] [-x MAX_HEAP]  [-q query] [-o] [-c] [-r RUNS] [-p PREHEAT_RUNS] [-v COMMIT] [-j] [-E] [-t TIMEOUT_MINS]\n"
  echo    "Options:"
  echo    "    -h Show this help message"
  echo    "    -x Set the maximum JVM heap size (-Xmx). Default is 2048M"
  echo    "    -q Run only the given query name (e.g., S2, B5, ...)"
  echo    "    -o Off the record: do not write to bench-riefederator-logs nor "
  echo    "       bench-riefederator.csv"
  echo    "    -c Append to bench-riefederator-logs and bench-riefederator.csv "
  echo    "       when a single query is given (-q)"
  echo    "    -r How many times to execute each query (within the same JVM)"
  echo    "       Default: $RUNS"
  echo    "    -p How many times to execute each query before starting the runs "
  echo    "       specified by -r. The results of these preheat runs will be discarded, "
  echo    "       but the cache will remain for subsequent executions of the same query. "
  echo    "       Default: $PREHEAT_RUNS"
  echo    "    -v Branch name or commit hash to checkout. Default: master"
  echo    "    -j For each run (-r) create a new JVM and do the preheat runs (-p) "
  echo    "       for each such JVM"
  echo    "    -t Timeout in minutes for a single run of an experiment (i.e., a query) "
  echo    "       With -p 2 and -r 1, the effective timeout for a child JVM will be 3 "
  echo    "       times the value given here. Default: 10"
  echo    "    -E Do not execute queries, only plan"
}

source "$DIR/scripts/lib.sh"

while getopts "hx:q:ocr:p:v:jEt:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    x)
      FEDX_XMX="$OPTARG"
      check_opt -p '[0-9]+[mMgG]?' $PREHEAT_RUNS
      ;;
    o)
      OTR=1
      ;;
    c)
      RECORD_SINGLE=1
      ;;
    q)
      QUERY_NAME="$OPTARG"
      ;;
    r)
      RUNS="$OPTARG"
      check_opt -r '[0-9]+' $RUNS
      ;;
    p)
      PREHEAT_RUNS="$OPTARG"
      check_opt -p '[0-9]+' $PREHEAT_RUNS
      ;;
    v)
      RIEF_COM="$OPTARG"
      ;;
    j)
      RIEF_CHILD=1
      ;;
    E)
      ONLYPLAN=1
      ;;
    t)
      RIEF_TOUT_MINS="$OPTARG"
      check_opt -p '[0-9]+' $RIEF_TOUT_MINS
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# No positional arguments
if [ "$#" -ge "$OPTIND" ]; then
  echo "Too many arguments ($0 takes no positional arguments)" 1>&2
  usage 1>&2
  exit 1
fi

# Clone & checkout commit
if [ ! -d "$RIEF_DIR" ]; then
  test "$RIEF_COM" == master && DEPTH_F="--depth=1" || DEPTH_F=""
  mkdir -p "$(dirname "$RIEF_DIR/riefederator")" || exit 1
  ( set -x ; git clone $DEPTH_F https://github.com/alexishuf/riefederator.git "$RIEF_DIR/riefederator" )
  if [ "$?" -ne 0 ]; then
    rm -fr "$RIEF_DIR/riefederator"; exit 1
  fi
  ( set -x ; cd "$RIEF_DIR/riefederator" && \
      git checkout "$RIEF_COM" )
fi
# Pull & compile
set -x
cd "$RIEF_DIR/riefederator" || exit 1
while ( git stash drop ); do true; done # Remove all stashes
STASHED=1
( git stash -m "pre-pull stash at $(date -I)" \
    | grep -i "No local changes" &>/dev/null ) && STASHED=0
git checkout "$RIEF_COM" || exit 1
git pull || exit 1
if [ "$STASHED" == 1 ]; then
  if ! ( git stash pop ); then
    echo "Failed to apply stash after pull!"
    exit 1
  fi
fi
rm -fr target
if ( ! ./mvnw package -DskipTests=true &> log ); then
  cat log
  echo "Failed to build riefederator Log: $(pwd)/log"
  exit 1
fi
set +x

# Define command lines
RIEF_CP="riefederator/riefederator-core/target/riefederator-core-1.0-SNAPSHOT.jar"
ADD_OPENS="--add-opens java.base/java.lang=ALL-UNNAMED"
if ( java --version | grep -E ' 8\.' &>/dev/null ); then
  ADD_OPENS=""
fi
test "$RIEF_CHILD" != 1 && RIEF_JAVA_XMX="-Xmx$RIEF_XMX" || RIEF_JAVA_XMX=""
RIEF_JAVA="java $RIEF_JAVA_XMX $ADD_OPENS -cp $RIEF_CP"
RIEF_QEV="$RIEF_JAVA br.ufsc.lapesd.riefederator.benchmark.QueryEvaluation"
RIEF_QRC="$RIEF_JAVA br.ufsc.lapesd.riefederator.benchmark.QueryResultsCleanup"

# Configure the federation
cd "$DIR"
if [ -z "$QUERY_NAME" ]; then
  echo "Cleaning $RIEF_DIR/cache, since we are running all queries"
  rm -fr "$RIEF_DIR/cache"
fi
mkdir -p "$RIEF_DIR/cache" # cache for endpoint descriptions
echo "sources-cache: cache" > "$RIEF_DIR/config.yaml"
echo "sources:" >> "$RIEF_DIR/config.yaml"
for uri in $("$DIR/config.sh" list_eps); do
  echo "  - loader: sparql" >> "$RIEF_DIR/config.yaml"
  echo "    uri: $uri" >> "$RIEF_DIR/config.yaml"
done
cp -r queries "$RIEF_DIR/"

# Generate options for QueryEvaluation
LOG_TMP=$(mktemp)
LOG=$LOG_TMP
ONLYPLAN_OPT=""
CSV_OPT=""
QRY_RESULTS_DIR=
QRY_RESULTS_OPT=
if [ "$OTR" != 1 ]  && ( [ -z "$QUERY_NAME" ] || [ "$RECORD_SINGLE" == 1 ] ); then
  LOG="$RESULTS/bench-riefederator.log"
  QRY_RESULTS_DIR="$RESULTS/bench-riefederator-results"
  QRY_RESULTS_OPT="--query-results-dir $QRY_RESULTS_DIR"
  mkdir -p "$QRY_RESULTS_DIR"
  CSV_OPT="--csv $RESULTS/bench-riefederator.csv --csv-append"
fi
test "$RIEF_CHILD" == 1 && CHILD_OPT="--child-jvm --child-xmx $RIEF_XMX" \
    || CHILD_OPT=
test "$ONLYPLAN" == 1 && ONLYPLAN_OPT="--only-plan"
QUERY_FILES=$(find queries -type f | sort)
test ! -z "$QUERY_NAME" && QUERY_FILES="queries/$QUERY_NAME"

# Cleanup stale query result files
if [ ! -z "$QRY_RESULTS_DIR" ]; then
  echo "Cleaning up stale query result files"
  ( set -x ; cd "$RIEF_DIR" ; \
    $RIEF_QRC --csv "$RESULTS/bench-riefederator.csv" \
              --query-results-dir "$QRY_RESULTS_DIR" )
fi

# Run riefederator
echo "######################################" >> "$LOG"
date -Iminutes >> "$LOG"
echo "--------------------------------------" >> "$LOG"
( set -x ; cd "$RIEF_DIR" ; \
  $RIEF_QEV --config config.yaml $CSV_OPT $CHILD_OPT $QRY_RESULTS_OPT \
            --preheat $PREHEAT_RUNS --runs $RUNS $ONLYPLAN_OPT \
            --timeout-mins $RIEF_TOUT_MINS \
            $QUERY_FILES 2>&1 | tee -a "$LOG" )
rm "$LOG_TMP"


