#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
ALL_ARGS=( "$0" "$@" )

usage() {
  echo -e "Usage: $0 [-h] [-q query] SYSTEM SYSTEM_ARGS\n"
  echo    "Options:"
  echo    "    -h Show this help message"
  echo -e "    -q Run a particular query given its name\n"
  echo    "Systems:"
  echo    "    fedx"
  echo    "    riefederator"
  echo    "Hint: use -h after SYSTEM to get SYSTEM-specific help"
}

while getopts "hq:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    q)
      QUERY_NAME="$OPTARG"
      ;;
  esac
done

# Parse DATASET_NAME
SYSTEM="$(echo ${ALL_ARGS[$OPTIND]} | sed -E 's/\.[a-zA-Z]+$//')"
if [ -z "$SYSTEM" ]; then
  echo "Missing SYSTEM" 1>&2
  usage 1>&2
  exit 1
fi
SYSTEM_ARGS=()
for i in $(seq $(($OPTIND + 1)) ${#ALL_ARGS[*]}); do
  SYSTEM_ARGS+=("${ALL_ARGS[$i]}")
done

# Checkout LargeRDFBench
if [ ! -d "$DIR/nobck/LargeRDFBench" ]; then
  mkdir -p "$DIR/nobck"
  cd "$DIR/nobck"
  ( set -x ; git clone --depth 1 https://github.com/dice-group/LargeRDFBench.git )
  if [ "$?" != 0 ]; then
    rm -fr "$DIR/nobck/LargeRDFBench" # remove incomplete clone
    exit 1
  fi
fi
# Always do a clean extraction of FedX & SPLENDID
( cd "$DIR/nobck/LargeRDFBench" && rm -fr FedX-* SPLENDID-* )
( set -x; cd "$DIR/nobck/LargeRDFBench/" && 7za x -y LargeRDFBench_Selected_Systems.7z )

SYSTEM_SCRIPT="$DIR/scripts/bench-$SYSTEM.sh"
"$SYSTEM_SCRIPT" ${SYSTEM_ARGS[*]}
