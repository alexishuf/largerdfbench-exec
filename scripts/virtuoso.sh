#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
DOWNLOAD_DIR=$HOME
REDOWNLOAD=0
ALL_ARGS=( "$0" "$@" )
MAX_QUERY_MEM=""
MACHINE_MEM=""
PID_FILE=""

usage() {
  echo -e "Usage: $0 [-h] [-d DOWNLOAD_DIR] [-q MaxQueryMem] [-m HOST_MEM] [-r] [-p PID_FILE] DATASET_NAME\n"
  echo    "Options:"
  echo    "    -h Show this help message"
  echo    "    -d Where to download and extract the Virtuoso archive. "
  echo    "       Default is $DOWNLOAD_DIR"
  echo    "    -q Override MaxQueryMem in virtuoso.ini. "
  echo    "       Default is determined by virtuoso.ini: 4G"
  echo    "    -m Override NumberOfBuffers and MaxDirtyBuffers in "
  echo    "       virtuoso.ini based on total system memory given to this option."
  echo    "       Default is determined by virtuoso.ini: 16G"
  echo    "    -r Force re-download and re-extract "
  echo    "    -p Run virtuoso in background, using the given PID file"
}

source "$DIR/virtuoso/lib.sh"

while getopts "hd:q:m:rp:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    d)
      DOWNLOAD_DIR="$OPTARG"
      ;;
    q)
      MAX_QUERY_MEM="$OPTARG"
      ;;
    m)
      MACHINE_MEM="$OPTARG"
      ;;
    r)
      REDOWNLOAD=1
      ;;
    p)
      PID_FILE="$OPTARG"
      remove_stale_pid_file "$PID_FILE" virtuoso-t
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# Parse DATASET_NAME
DATASET_NAME="$(echo ${ALL_ARGS[$OPTIND]} | sed -E 's/\.[a-zA-Z]+$//')"
if [ -z "$DATASET_NAME" ]; then
  echo "Missing DATASET_NAME" 1>&2
  usage 1>&2
  exit 1
fi
if [ "${#ALL_ARGS[*]}" -gt $(($OPTIND + 2)) ]; then
  echo "Extra args after DATASET_NAME=$DATASET_NAME" 1>&2
  exit 1
fi
DATASET_DIR="$DOWNLOAD_DIR/$DATASET_NAME-virtuoso-7.1-64bit-linux"
DATASET_FILE="$DATASET_DIR.7z"

# Process MACHINE_MEM
case $MACHINE_MEM in
  2G)
    NUMBER_OF_BUFFERS=170000
    MAX_DIRTY_BUFFERS=130000
    ;;
  4G)
    NUMBER_OF_BUFFERS=340000
    MAX_DIRTY_BUFFERS=250000
    ;;
  8G)
    NUMBER_OF_BUFFERS=680000
    MAX_DIRTY_BUFFERS=500000
    ;;
  16G)
    NUMBER_OF_BUFFERS=1360000
    MAX_DIRTY_BUFFERS=1000000
    ;;
  *)
    echo "Bad value for MACHINE_MEM (-m): $MACHINE_MEM. Expected 2G, 4G, 8G or 16G" 1>&2
    exit 1
    ;;
esac

# Show relevant environment
echo "+--------------------------------"
echo "|  DATASET_NAME: $DATASET_NAME"
echo "|   DATASET_DIR: $DATASET_DIR"
echo "| MaxQueryMem: $MAX_QUERY_MEM"
echo "|   MACHINE_MEM: $MACHINE_MEM"
echo "| NumberOfBuffers: $NUMBER_OF_BUFFERS"
echo "| MaxDirtyBuffers: $MAX_DIRTY_BUFFERS"
echo "+--------------------------------"


# Download & extract virtuoso
if [ "$REDOWNLOAD" = 1 ]; then
  sleep_rm "$DATASET_DIR"
  sleep_rm "$DATASET_FILE"
fi
GD_ID="$($DIR/config.sh virtuoso_gd_id "$DATASET_NAME")"
if [ "$?" != 0 ]; then
  echo "No Google drive ID for dataset $DATASET_NAME"; exit 1;
fi
mkdir -p "$DOWNLOAD_DIR" && cd "$DOWNLOAD_DIR" || exit 1
if [ ! -f "$DATASET_FILE" ]; then
  set -x; "$DIR/scripts/gdrive-download.sh" "$GD_ID" "$DATASET_FILE" || exit 1; set +x
fi
# Names of the dir inside the archive not always match $DATASET_NAME
# Fix this during extraction
if [ ! -d "$DATASET_DIR" ]; then
  set -x
  T="$(mktemp)"
  T_DIR="$(basename "$T")"
  rm -fr "$T"
  mkdir "$T_DIR" && cd "$T_DIR"
  7za x "$DATASET_FILE" || exit 1
  set +x
  if [ "$(ls | wc -l)" != 1 ]; then
    echo "Bad virtuoso archive: expected one directory, got $(ls)." 1>&2
    exit 1
  fi
  set -x
  mv * "$DATASET_DIR" || exit 1
  cd ..
  rm -fr "$T_DIR" || exit 1
  set +x
fi

# Customize configuraion
VIRTUOSO_INI="../var/lib/virtuoso/db/virtuoso.ini"
T_INI1="$(mktemp)"
T_INI2="$(mktemp)"
set -x
cd "$DATASET_DIR/bin" || exit 1
cp "$VIRTUOSO_INI" "$T_INI1" || exit 1
if [ ! -z "$MAX_QUERY_MEM" ]; then
  sed -E "s/(MaxQueryMem *=) *[0-9]+G/\1 $MAX_QUERY_MEM/g" "$T_INI1" > "$T_INI2"
  cp "$T_INI2" "$T_INI1"
fi
if [ ! -z "$MACHINE_MEM" ]; then
  sed -E "s/(^ *NumberOfBuffers *=) *[0-9]+([^0-9]*|$)/\1 $NUMBER_OF_BUFFERS/g" "$T_INI1" > "$T_INI2"
  cp "$T_INI2" "$T_INI1"
  sed -E "s/^ *(MaxDirtyBuffers *=) *[0-9]+([^0-9]*|$)/\1 $MAX_DIRTY_BUFFERS/g" "$T_INI1" > "$T_INI2"
  cp "$T_INI2" "$T_INI1"
fi
set +x;
diff "$VIRTUOSO_INI" "$T_INI1"
cp "$T_INI1" "$VIRTUOSO_INI"

# Start virtuoso
if [ -z "$PID_FILE" ]; then
  ./start_virtuoso.sh
else
  test  -f "$PID_FILE" && kill_pid $(cat "$PID_FILE") virtuoso-t 20 5
  ./virtuoso-t -fd -c ../var/lib/virtuoso/db/virtuoso.ini &
  PID=$!
  echo $PID > "$PID_FILE"
  disown $PID
  exit 0
fi
