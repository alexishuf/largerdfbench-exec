#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."

usage() {
  echo -e "Usage: $0 PID_FILE\n"
}

source "$DIR/scripts/lib.sh"

# Parse argument
PID_FILE=$1
if [ -z "$PID_FILE" ]; then
  usage
  exit 1
fi

# Wait
while [ ! -f "$PID_FILE" ]; do
  sleep 0.25s
done
exit 0

