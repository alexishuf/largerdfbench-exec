#!/bin/bash
FILEID=$1
FILE_NAME=$2

if [ -z "$FILEID" -a -z "$FILE_NAME" ] ; then
   echo "Usage: $0 FILE_ID OUTPUT_FILE"
   exit 1
fi

COOKIE=$(mktemp)
curl -c ${COOKIE} -s -L "https://drive.google.com/uc?export=download&id=${FILEID}" > /dev/null
curl -Lb ${COOKIE} "https://drive.google.com/uc?export=download&confirm=$(awk '/download/ {print $NF}' ${COOKIE})&id=${FILEID}" -o ${FILE_NAME}
rm "$COOKIE"
