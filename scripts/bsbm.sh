#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
ALL_ARGS=( "$0" "$@" )
BSBM_DIR="$HOME/bsbm"
WGET=wget

usage() {
  echo -e "Usage: $0 [-h4] -g GENERATOR_DIR -o OUT_FILE DATASET_SPEC\n"
  echo    "Options:"
  echo    "    -h Show this help message\n"
  echo    "    -4 Force wget to use IPv4"
  echo    "    -g Dir where to download and extract bsbm dataset generator."
  echo    "       Default: $HOME"
  echo    "    -o Destination file where to write the dataset in Turtle syntax."
  echo    "       Default $HOME/DATASET_NAME.ttl where DATASET_NAME is the last "
  echo    "       segment of the :-separated DATASET_SPEC"
}

source "$DIR/scripts/lib.sh"

while getopts "h4g:o:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    4)
      WGET="wget -4"
      ;;
    g)
      BSBM_DIR="$OPTARG"
      ;;
    o)
      OUT_FILE="$OPTARG"
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# Take positional argument
DATASET_SPEC="$(echo ${ALL_ARGS[$OPTIND]} | sed -E 's/\.[a-zA-Z]+$//')"
if [ -z "$DATASET_SPEC" ]; then
  echo "Missing DATASET_SPEC" 1>&2
  usage 1>&2
  exit 1
fi
if [ "${#ALL_ARGS[*]}" -gt $(($OPTIND + 2)) ]; then
  echo "Extra args after DATASET_SPEC=$DATASET_SPEC" 1>&2
  exit 1
fi

# Parse DATASET_SPEC
DATASET_SPEC="$(echo "$DATASET_SPEC" | sed 's/^bsbm://g')"
RX='^([0-9]+):([a-zA-z]+)$'
N_PRODUCTS="$(echo   "$DATASET_SPEC" | sed -E "s/${RX}/\1/g")"
DATASET_NAME="$(echo "$DATASET_SPEC" | sed -E "s/${RX}/\2/g")"
if [ -z "$N_PRODUCTS" -o -z "$DATASET_NAME" ]; then
  echo "Invalid DATASET_SPEC $DATASET_SPEC"
  exit 1
fi

# Compute default OUT_FILE
if [ -z "$OUT_FILE" ]; then
  OUT_FILE="$HOME/$DATASET_NAME.ttl"
fi

# Show relevant environment
echo "+-----------[ bsbm.sh env ]-----------"
echo "|      OUT_FILE: $OUT_FILE"
echo "|     BSBM_DIR: $BSBM_DIR"
echo "|   N_PRODUCTS: $N_PRODUCTS"
echo "| DATASET_NAME: $DATASET_NAME"
echo "|         WGET: $WGET"
echo "+-------------------------------------"

# Create dir containing OUT_FILE
( set -x; mkdir -p "$(dirname $OUT_FILE)" ) || exit 1

# Download BSBM generator
( set -x ; mkdir -p "$BSBM_DIR" ) || exit 1
cd "$BSBM_DIR" || exit 1
if [ -f bsbmtools-v0.2.zip ]; then
  if ! ( sha512sum -c "$DIR/bsbmtools-v0.2.zip.sha512" ); then
    echo "Bad checksum for bsbmtools-v0.2.zip. Will re-download"
    rm bsbmtools-v0.2.zip
  fi
fi
if [ ! -f bsbmtools-v0.2.zip ]; then
  TIMESTAMP=$(echo $(date +%s) | grep -E '^[0-9]+$')
  if [ -z "$TIMESTAMP" ]; then
    echo "date +%s didn't generate a timestamp in seconds from epoch as expected"
    exit 1
  fi
  ( set -x ; $WGET -O bsbmtools-v0.2.zip "https://downloads.sourceforge.net/project/bsbmtools/bsbmtools/bsbmtools-0.2/bsbmtools-v0.2.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fbsbmtools%2Ffiles%2Flatest%2Fdownload&ts=$TIMESTAMP" ) || exit 1
  if ! ( sha512sum -c "$DIR/bsbmtools-v0.2.zip.sha512" ); then
    echo "Checksum of downloaded bsbmtools-v0.2.zip differs from expected"
    exit 1
  fi
fi
rm -fr bsbmtools-0.2
( set -x ; unzip bsbmtools-v0.2.zip ) || exit 1

# Cleanup generator side and create FIFO
cd "$BSBM_DIR/bsbmtools-0.2/" || exit 1
rm -fr test_driver dataset.nt
( set -x ; mkdir -p test_driver ) || exit 1
FIFO_FILE=$(pwd)/dataset.nt
( set -x ; mkfifo "$FIFO_FILE" ) || exit 1

# Start dataset generator
GENERATOR_LOG=$(mktemp)
set -x ; ./generate -s nt -fc -pc $N_PRODUCTS -dir test_driver &>"$GENERATOR_LOG" &
GENERATOR_PID=$!
set +x


# Build Java utility to split the dataset
cd $DIR/bsbm-splitter
LOG=$(mktemp)
echo "Building bsbm-splitter JAR"
if ! ( set -x ; ./mvnw package &>"$LOG" ) ; then
  cat "$LOG" 1>&2
  echo "Failed to package bsbm-splitter"
  exit 1
fi
rm "$LOG"
BSBM_SPLITTER_JAR="$DIR/bsbm-splitter/target/bsbm-splitter-1.0-SNAPSHOT.jar"

# Check if the generator did not fail prematurely
if ! is_running $GENERATOR_PID bsbm.jar; then
  echo "Generator died prematurely. See the log:"
  cat "$GENERATOR_LOG"
  exit 1
fi

# Start Java utility and connect to the FIFO
OUT_DIR=$(dirname "$OUT_FILE")
OUT_NAME=$(echo $OUT_FILE | sed -E 's@^.*/([^/]+)$@\1@' | sed -E 's/\.ttl$//')
( set -x ; mkdir -p "$OUT_DIR" ) || exit 1
( set -x ; rm -f $OUT_FILE ) || exit 1
( set -x ; java -jar "$BSBM_SPLITTER_JAR" --only-class $DATASET_NAME --out-dir "$OUT_DIR" --only-out-name "$OUT_NAME" "$FIFO_FILE" )

# Wait for generator and clean up
echo "Done splitting, waiting for generator..."
wait $GENERATOR_PID
echo "Generator log:"
cat "$GENERATOR_LOG"
rm -f "$FIFO_FILE" "$GENERATOR_LOG"

# Check result file exists
if [ ! -f "$OUT_FILE" ]; then
  echo "Dataset file $OUT_FILE somehow does not exist!"
  exit 1
fi
echo "Wrote dataset file to $OUT_FILE"
