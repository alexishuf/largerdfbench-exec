#!/bin/bash

function sleep_rm () {
    if [ -e "$1" ]; then
      echo -e "\n    WARNING: Will REMOVE $1 in 2 seconds"
      sleep 2s
      test "$?" -eq 0 || exit 1
      rm -fr "$1"
  fi
}

function check_opt() {
  if ! ( echo $3 | grep -E "^$2\$" >/dev/null ); then
    echo "Bad value for $1: $3. Expected $2." 1>&2
    exit 1
  fi
}

function is_running() {
  PID=$1
  CMD=$2
  ps ax | grep -E "^[ \t]*$PID" | grep -E "$CMD" &>/dev/null
}

function wait_for_pid() {
  PID=$1
  CMD=$2
  SECONDS=$3
  for i in $(seq 1 $((4*$SECONDS))); do
    if ! is_running $PID "$CMD" ; then
      break
    fi
    sleep 0.250s
  done
  if is_running $PID "$CMD" ; then
    return 1
  fi
  return 0
}

function kill_pid() {
  PID=$1
  CMD=$2
  KILL_SECONDS=$3
  TERM_SECONDS=$4
  if is_running $PID "$CMD"; then
    echo "$CMD running. Sending SIGKILL and waiting for at most ${KILL_SECONDS}s"
    ( set -x ; kill $PID )
    if ! wait_for_pid $PID "$CMD" $KILL_SECONDS; then
      echo "Die-hard $CMD. Sending SIGTERM and waiting for ${TERM_SECONDS}s"
      ( set -x ; kill -9 $PID )
      if ! wait_for_pid $PID "$CMD" $TERM_SECONDS; then
        echo "$CMD survived SIGTERM for ${TERM_SECONDS}s. Will leave it be"
        return 1
      fi
    fi
  else
    echo "No $CMD running at PID $PID"
  fi
  return 0
}

function remove_stale_pid_file() {
  PID_FILE=$1
  CMD=$2
  if [ -f "$PID_FILE" ]; then
    is_running $(cat "$PID_FILE") virtuoso-t || \
      ( echo "Removing stale PID file $PID_FILE" ; set -x ; rm -f "$PID_FILE" )
  fi
}
