#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
RESULTS="$DIR/results"
ALL_ARGS=( "$0" "$@" )
FEDX_XMX=1024M
FEDX_DIR="$DIR/nobck/LargeRDFBench/FedX-3.1"
OTR=0
RECORD_SINGLE=0
RUNS=1
PREHEAT_RUNS=0
ONLYPLAN=0
TIMEOUT_MINS=5

usage() {
  echo -e "Usage: $0 [-h] [-x MAX_HEAP]  [-q query] [-o] [-c] [-r RUNS] [-p PREHEAT_RUNS] [-E] [-t TIMEOUT_SECS] \n"
  echo    "Options:"
  echo    "    -h Show this help message"
  echo    "    -x Set the maximum JVM heap size (-Xmx). Default is 1024M"
  echo    "    -q Run only the given query name (e.g., S2, B5, ...)"
  echo    "    -o Off the record: do not write to bench-fedx-logs nor "
  echo    "       bench-fedx.csv"
  echo    "    -c Append to bench-fedx-logs and bench-fedx.csv even when "
  echo    "       -o is given"
  echo    "    -r How many times to execute each query (within the same JVM)"
  echo    "       Default: $RUNS"
  echo    "    -p How many times to execute each query before starting the runs "
  echo    "       specified by -r. The results of these preheat runs will be discarded, "
  echo    "       but the cache will remain for subsequent executions of the same query. "
  echo    "       Default: $PREHEAT_RUNS"
  echo    "    -E Do not execute the queries, only plan"
  echo    "    -t Timeout minutes for processing a single query. With -t 10, -p 2 and "
  echo    "       -r 3, the effective timeout for a FedX instance will be (2+3)*10 minutes."
  echo    "       If FedX is killed due to timeout, runs which did not occur will store "
  echo    "       empty values (NAs) as results. Default is $TIMEOUT_MINS"
}

source "$DIR/scripts/lib.sh"

function log() {
  echo "[fedx-bench][$(date -Iminutes)] " $@ 1>&2
}

# queryMs covers query parsing, source selection, planning and execution
# this is the original measure of FedX in LargeRDFBench.
# other components: planMs and execMs refer to the specific phases
# obs: planMs does not include selMs
# obs: runs does not include preheatRuns
CSV_HEADERS="queryName,timestamp,preheatRuns,runs,askRequests,nSources,nResults,selMs,planMs,optMs,execMs,firstResultMs,queryMs"
function log2row() {
  # Check inputs
  LOG=$1
  QNAME=$2
  if [ -z "$LOG" ]; then
    echo "Empty LOG_FILE. Usage: log2row LOG_FILE QNAME" 1>&2 ; exit 1
  fi
  if [ ! -f "$LOG" ]; then
    echo "Log file $LOG does not exist" 1>&2 ; exit 1
  fi
  if [ -z "$QNAME" ]; then
    echo "Empty QNAME. Usage: log2row LOG_FILE QNAME" 1>&2 ; exit 1
  fi

  # Collect result lists into files
  ASK=$(mktemp)
  NSRCS=$(mktemp)
  NRES=$(mktemp)
  SEL=$(mktemp)
  PLAN=$(mktemp)
  OPT=$(mktemp)
  EXEC=$(mktemp)
  FRES=$(mktemp)
  QUERY=$(mktemp)
  sed -nE 's/^Number *of *ASK *requests? *: *([0-9]+) *$/\1/pi' "$LOG" > "$ASK"
  sed -nE 's/^Total Triple Pattern[- ]wise selected sources? *: *([0-9]+) *$/\1/pi' "$LOG" > "$NSRCS"
  sed -nE 's/^Total *Number *of *Records? *: *([0-9]+) *$/\1/pi' "$LOG" > "$NRES"
  sed -nE 's/^Source selection time *: *([0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$SEL"
  sed -nE 's/^FULL_PLAN_MS *: *([0-9]+.[0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$PLAN"
  sed -nE 's/^Opti?mization *time *: *([0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$OPT"
  sed -nE 's/^Query *execution *time *\(msec\) *: *([0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$EXEC"
  sed -nE 's/^FIRST_RESULT_MS *: *([0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$FRES"
  sed -nE 's/^Query *total *time *\(msec\) *: *([0-9]+.?[0-9]*) *$/\1/pi' "$LOG" > "$QUERY"

  for i in $(seq $RUNS -1 1); do
    echo -n "$QNAME,$(date -Iminutes),$PREHEAT_RUNS,$RUNS,"
    echo -n "$(tail -n $i $ASK   | head -n 1),"
    echo -n "$(tail -n $i $NSRCS | head -n 1),"
    echo -n "$(tail -n $i $NRES  | head -n 1),"
    echo -n "$(tail -n $i $SEL   | head -n 1),"
    echo -n "$(tail -n $i $PLAN  | head -n 1),"
    echo -n "$(tail -n $i $OPT   | head -n 1),"
    echo -n "$(tail -n $i $EXEC  | head -n 1),"
    echo -n "$(tail -n $i $FRES  | head -n 1),"
    echo    "$(tail -n $i $QUERY | head -n 1)"
  done

  # Clean up temp files
  rm -f "$ASK" "$NSRCS" "$NRES" "$SEL" "$PLAN" "$OPT" "$EXEC" "$FRES" "$QUERY" 
}

function record() {
  QNAME=$1
  LOG=$2
  echo "######################################" >> "$RESULTS/bench-fedx-logs/$QNAME"
  date -Iminutes >> "$RESULTS/bench-fedx-logs/$QNAME"
  echo "--------------------------------------" >> "$RESULTS/bench-fedx-logs/$QNAME"
  cat "$LOG" >> "$RESULTS/bench-fedx-logs/$QNAME"
  if [ ! -f "$RESULTS/bench-fedx.csv" ]; then
    echo "$CSV_HEADERS" >> "$RESULTS/bench-fedx.csv"
  fi
  log2row "$LOG" "$QNAME" >> "$RESULTS/bench-fedx.csv"
  log "Wrote results for query $QNAME to $RESULTS/bench-fedx.csv"
}

function tout_run() {
  if ! ( echo "$TIMEOUT_SECS" | grep -E '[0-9]+' &>/dev/null ); then
    echo "Bad value for \$TIMEOUT_SECS: $TIMEOUT_SECS".
    exit 1
  fi
  ATTEMPTS=$(($TIMEOUT_SECS*4))
  echo "+ $@"
  "$@" & 
  PID=$!
  for i in $(seq $ATTEMPTS); do
    sleep 0.25s
    ps -p $PID >/dev/null || break
  done
  if ( ps -p $PID >/dev/null ); then
    echo "Timeout for PID=$PID $@"
    (set -x ; kill $PID)
    for i in $(seq 20); do
      sleep 0.25s
      ps -p $PID >/dev/null || break
    done
    (set -x ; kill $PID)
    if ( ps -p $PID >/dev/null ); then
      echo "Die hard PID=$PID $@..."
      (set -x ; kill -9 $PID)
      for i in $(seq 20); do
        sleep 0.25s
        ps -p $PID >/dev/null || break
      done
      echo "PID=$PID survived kill -9. will send again and give up"
      (set -x ; kill -9 $PID)
      sleep 2s
      return 2 #Timeout
    fi
  fi
  wait $PID # safe, since we know $PID is dead
  return $? # return exit code of "$@"
}

while getopts "hq:ocr:p:Et:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    x)
      FEDX_XMX="$OPTARG"
      check_opt -p '[0-9]+[mMgG]?' $PREHEAT_RUNS
      ;;
    o)
      OTR=1
      ;;
    c)
      RECORD_SINGLE=1
      ;;
    q)
      QUERY_NAME="$OPTARG"
      ;;
    r)
      RUNS="$OPTARG"
      check_opt -r '[0-9]+' $RUNS
      ;;
    p)
      PREHEAT_RUNS="$OPTARG"
      check_opt -p '[0-9]+' $PREHEAT_RUNS
      ;;
    E)
      ONLYPLAN=1
      ;;
    t)
      TIMEOUT_MINS="$OPTARG"
      check_opt -t '[0-9]+' $TIMEOUT_MINS
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# Compute effective timeout
TIMEOUT_SECS=$(( ${TIMEOUT_MINS} * 60 * $((${PREHEAT_RUNS}+${RUNS})) ))

# No positional arguments
if [ "$#" -ge "$OPTIND" ]; then
  echo "Too many arguments ($0 takes no positional arguments)" 1>&2
  usage 1>&2
  exit 1
fi

# Check for packages
if  ! ( dos2unix --version &>/dev/null ); then
  echo "dos2unix required but not found in the \$PATH"
  exit 1
fi
if  ! ( javac --version  &>/dev/null ); then
  echo "javac required but not found in the \$PATH"
  exit 1
fi
if  ! ( patch --version  &>/dev/null ); then
  echo "patch required but not found in the \$PATH"
  exit 1
fi

# Define command lines
FEDX_CP=bin
cd "$FEDX_DIR"
for lib in $(find lib/ -type f); do
  FEDX_CP+=:$lib
done
cd "$DIR"
FEDX_JAVA="java -Xmx$FEDX_XMX -cp $FEDX_CP"
FEDX_QEV="$FEDX_JAVA org.aksw.simba.start.QueryEvaluation"

# Copy data and configurations from the top-level
rm -fr "$FEDX_DIR/queries"
cp -r queries "$FEDX_DIR/"
"$DIR/config.sh" list_eps > "$FEDX_DIR/endpoints"

if [ "$OTR" != 1 ]; then
  mkdir -p $RESULTS/bench-fedx-logs
fi

# Patch FedX
set -x
cd "$FEDX_DIR"
find src -name '*.java' -exec dos2unix '{}' \;
patch -p0 -s < "$DIR/patch/FedX-3.1.patch" || exit 1
javac -cp "$FEDX_CP" -d bin/ \
      src/com/fluidops/fedx/optimizer/Optimizer.java \
      src/org/aksw/simba/start/QueryEvaluation.java || exit 1
set +x

TOTAL_RUNS="$(($PREHEAT_RUNS + $RUNS))"
test "$ONLYPLAN" == 1 && ONLYPLAN_OPT="ONLY_PLAN" || ONLYPLAN_OPT=
TMP_LOG=$(mktemp)
if [ -z $QUERY_NAME ]; then
  for qpath in queries/*; do
    qname=$(echo $qpath | sed 's@queries/@@')
    log "Running query $qname $PREHEAT_RUNS+$RUNS times in a single JVM..."
    rm -f cache.db ; tout_run $FEDX_QEV "$qname" $TOTAL_RUNS $ONLYPLAN_OPT \
                                          2>&1 | tee "$TMP_LOG" 
    test "$OTR" == 1 || record "$qname" "$TMP_LOG"
  done
else
  log "Running query $QUERY_NAME $PREHEAT_RUNS+$RUNS times in a single JVM..."
  rm -f cache.db ; tout_run $FEDX_QEV $QUERY_NAME $TOTAL_RUNS $ONLYPLAN_OPT  \
                                        2>&1 | tee "$TMP_LOG" 
  test "$OTR" == 1 -o "$RECORD_SINGLE" == 0 || record "$QUERY_NAME" "$TMP_LOG"
fi
rm "$TMP_LOG"

