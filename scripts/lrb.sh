#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
DOWNLOAD_DIR=$HOME
DUMPS_REDOWNLOAD=0
EXTRACT_DIR=$HOME
ALL_ARGS=( "$0" "$@" )

usage() {
  echo -e "Usage: $0 [-hf] [-d DOWNLOAD_DIR] [-o EXTRACT_DIR] DATASET_NAME\n"
  echo    "Options:"
  echo    "    -h Show this help message\n"
  echo    "    -f Force dumps re-download"
  echo    "    -d Where to download compressed dumps. Default is $HOME"
  echo    "    -o Where to extract compressed dumps. Since LRB dumps for"
  echo    "       dataset X contains a directory named X, the RDF files will "
  echo    "       be located in DOWNLOAD_DIR/X/. Default for -d DOWNLOAD_DIR "
  echo    "       is $HOME"
}

source "$DIR/scripts/lib.sh"

while getopts "hfd:o:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    f)
      DUMPS_REDOWNLOAD=1
      ;;
    d)
      DOWNLOAD_DIR="$OPTARG"
      ;;
    o)
      EXTRACT_DIR="$OPTARG"
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# Get DATASET_NAME
DATASET_NAME="$(echo ${ALL_ARGS[$OPTIND]} | sed -E 's/\.[a-zA-Z]+$//')"
if [ -z "$DATASET_NAME" ]; then
  echo "Missing DATASET_NAME" 1>&2
  usage 1>&2
  exit 1
fi
if [ "${#ALL_ARGS[*]}" -gt $(($OPTIND + 2)) ]; then
  echo "Extra args after DATASET_NAME=$DATASET_NAME" 1>&2
  exit 1
fi
DATASET_EXT="$($DIR/config.sh ext $DATASET_NAME)"
if [ "$?" != 0 ]; then
  echo "Problem with config.sh"; exit 1;
fi

# Show relevant environment
echo "+-----------[ lrb.sh env ]-----------"
echo "|     DOWNLOAD_DIR: $DOWNLOAD_DIR"
echo "|      EXTRACT_DIR: $EXTRACT_DIR"
echo "|     DATASET_NAME: $DATASET_NAME"
echo "|      DATASET_EXT: $DATASET_EXT"
echo "| DUMPS_REDOWNLOAD: $DUMPS_REDOWNLOAD"
echo "+------------------------------------"

# Download dump
GD_ID="$($DIR/config.sh gd_id "$DATASET_NAME")"
if [ "$?" != 0 ]; then
  echo "No Google drive ID for dataset $DATASET_NAME"; exit 1;
fi
DATASET_FILE="$DATASET_NAME.$DATASET_EXT"
( set -x ; mkdir -p "$DUMPS_REDOWNLOAD" ) || exit 1
cd "$DOWNLOAD_DIR" || exit 1
if [ "$DUMPS_REDOWNLOAD" == 1 ]; then
  sleep_rm "$DOWNLOAD_DIR/$DATASET_FILE"
  sleep_rm "$DOWNLOAD_DIR/$DATASET_NAME"
fi
if [ ! -f "$DATASET_FILE" ]; then
  ( set -x ; "$DIR/scripts/gdrive-download.sh" "$GD_ID" "$DATASET_FILE" ) || exit 1
fi

# Extract and patch dump
if [ ! -d "$DATASET_NAME" ]; then
  ( set -x ; 7za x "$DATASET_FILE" ) || exit 1
  PATCH_FILE="$DIR/patch/$DATASET_NAME.patch"
  if [ -f "$PATCH_FILE" ]; then
    # Convert CRLF (from some datasets) into LF, (used in patches)
    ( set -x ;  find "$DATASET_NAME" -name '*.*' -exec dos2unix '{}' \; )
    ( set -x ; patch -p0 -s < "$PATCH_FILE" ) || exit 1
  fi
fi
