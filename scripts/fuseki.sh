#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
FUSEKI_DOWNLOAD=${FUSEKI_DOWNLOAD:-$HOME}
BSBM_DOWNLOAD=${BSBM_DOWNLOAD:-$HOME}
FUSEKI_SERVER=${FUSEKI_SERVER:-$FUSEKI_DOWNLOAD/apache-jena-fuseki-3.14.0/fuseki-server}
FUSEKI_HEAP=1200M
JENA_PATH=${JENA_PATH:-$FUSEKI_DOWNLOAD/apache-jena-3.14.0/bin}
TDB_LOC_PREFIX=${TDB_LOC_PREFIX:-$HOME/tdb}
TDB_VER_FLAG="--tdb2"
TDB_RELOAD=0
ALL_ARGS=( "$0" "$@" )
WGET=wget
DUMPS_REDOWNLOAD_OPT=
FORCEIP4=
PID_FILE=

usage() {
  echo -e "Usage: $0 [-h] [-x maxHeap] [-4] [-b BSBM_DOWNLOAD] [-f FUSEKI_SERVER_PATH] [-F FUSEKI_DOWNLOAD_PATH] [-l TDB_LOCATION] [-r] [-R] [-j JENA_PATH] [-1] [-p PID_FILE] DATASET_NAME\n"
  echo    "Options:"
  echo    "    -h Show this help message\n"
  echo    "    -x Maximum heap usage, as given to -Xmx. Default is $FUSEKI_HEAP"
  echo    "    -4 Passed to wget: makes it force IPv4 address"
  echo    "    -b Where to download the BSBM dataset generator. "
  echo    "       Default is $BSBM_DOWNLOAD"
  echo    "    -f Path to the fuseki-server binary. Default is guessed from -F"
  echo    "    -F Where to download and extract Jena Fuseki binary"
  echo    "       distribution. The download will occur only if -f is not "
  echo    "       given. Default is $FUSEKI_DOWNLOAD"
  echo    "    -l Where to create TDB/TDB2 storage. "
  echo    "       Default is TDB_LOC=$TDB_LOC_PREFIX/\$DATASET_NAME"
  echo    "    -r Re-create TDB storage from dumps"
  echo    "    -R Re-download and re-extract dumps"
  echo    "    -j Path to tdb binaries. "
  echo    "       Default: $FUSEKI_DOWNLOAD/apache-jena-3.14.0/bin "
  echo    "    -d Where to download dumps. Default is $HOME"
  echo    "    -1 Use TDB 1 instead of the default, TDB2"
  echo    "    -p Run Fuski in background, using the given PID file"
}

source "$DIR/scripts/lib.sh"

while getopts "hx:4b:f:F:l:rRj:d:1p:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    x)
      FUSEKI_HEAP="$OPTARG"
      ;;
    4)
      WGET="wget -4"
      FORCEIP4="-4"
      ;;
    b)
      BSBM_DOWNLOAD="$OPTARG"
      ;;
    f)
      FUSEKI_SERVER="$OPTARG"
      ;;
    F)
      FUSEKI_DOWNLOAD="$OPTARG"
      ;;
    l)
      TDB_LOC="$OPTARG"
      ;;
    r)
      TDB_RELOAD=1
      ;;
    R)
      DUMPS_REDOWNLOAD_OPT=-f
      ;;
    j)
      JENA_PATH="$OPTARG"
      ;;
    d)
      DUMP_DOWNLOAD="$OPTARG"
      ;;
    1)
      TDB_VER_FLAG=""
      ;;
    p)
      PID_FILE="$OPTARG"
      remove_stale_pid_file "$PID_FILE" fuseki-server.jar
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# Parse DATASET_NAME
DATASET_NAME="$(echo ${ALL_ARGS[$OPTIND]} | sed -E 's/\.[a-zA-Z]+$//')"
if [ -z "$DATASET_NAME" ]; then
  echo "Missing DATASET_NAME" 1>&2
  usage 1>&2
  exit 1
fi
if [ "${#ALL_ARGS[*]}" -gt $(($OPTIND + 2)) ]; then
  echo "Extra args after DATASET_NAME=$DATASET_NAME" 1>&2
  exit 1
fi

# Fill in defaults
if [ -z "$TDB_LOC" ]; then
  TDB_LOC="$TDB_LOC_PREFIX/$DATASET_NAME"
fi
if [ -z "$DUMP_DOWNLOAD" ]; then
   DUMP_DOWNLOAD="$HOME"
fi

# Show relevant environment
echo "+--------------------------------"
echo "|    DATASET_NAME: $DATASET_NAME"
echo "|       JENA_PATH: $JENA_PATH"
echo "|   FUSEKI_SERVER: $FUSEKI_SERVER"
echo "| FUSEKI_DOWNLOAD: $FUSEKI_DOWNLOAD"
echo "|   DUMP_DOWNLOAD: $DUMP_DOWNLOAD"
echo "|         TDB_LOC: $TDB_LOC"
echo "+--------------------------------"

# Download Fuseki
if [ -x "$FUSEKI_SERVER" ]; then
  echo "FUSEKI_SERVER=$FUSEKI_SERVER exists and is executable"
else
  FUSEKI_SERVER="$FUSEKI_DOWNLOAD/apache-jena-fuseki-3.14.0/fuseki-server"
  if [ ! -x "$FUSEKI_SERVER" ]; then
    set -x
    mkdir -p "$FUSEKI_DOWNLOAD" || exit 1
    cd "$FUSEKI_DOWNLOAD" || exit 1
    $WGET -c  https://archive.apache.org/dist/jena/binaries/apache-jena-fuseki-3.14.0.tar.gz || exit 1
    sha512sum -c "$DIR/apache-jena-fuseki-3.14.0.tar.gz.sha512" || exit 1
    tar zxf apache-jena-fuseki-3.14.0.tar.gz || exit 1
    chmod +x "$FUSEKI_SERVER" || exit 1
    set +x
  else
    echo "Using $FUSEKI_SERVER"
  fi
fi

# Download Jena
if [ -x "$JENA_PATH/tdb2.tdbloader" ]; then
  echo "JENA_PATH=$JENA_PATH looks good"
else
  JENA_PATH="$FUSEKI_DOWNLOAD/apache-jena-3.14.0/bin"
  if [ ! -x "$JENA_PATH/tdb2.tdbloader" ]; then
    set -x
    mkdir -p "$FUSEKI_DOWNLOAD" || exit 1
    cd "$FUSEKI_DOWNLOAD" || exit 1
    $WGET -c https://archive.apache.org/dist/jena/binaries/apache-jena-3.14.0.tar.gz || exit 1
    sha512sum -c "$DIR/apache-jena-3.14.0.tar.gz.sha512" || exit 1
    tar zxf apache-jena-3.14.0.tar.gz || exit 1
    set +x
  fi
fi

TDB_LOADER="$JENA_PATH/tdb2.tdbloader"
if [ -z "$TDB_VER_FLAG" ]; then
  TDB_LOADER="$JENA_PATH/tdbloader2"
fi
echo "Using TDB_LOADER=$TDB_LOADER"

# Download & extract dump
( set -x ; mkdir -p "$DUMP_DOWNLOAD" ) || exit 1
cd "$DUMP_DOWNLOAD" || exit 1
PORT=8886
DS_FILES=
FUSEKI_DS_NAME="$DATASET_NAME"
if ( echo $DATASET_NAME | grep -E '^bsbm:' ); then
  BSBM_CLASS=$(echo "$DATASET_NAME" | sed -E 's/^.*:([^:]+)$/\1/g')
  FUSEKI_DS_NAME="$BSBM_CLASS"
  PORT="$($DIR/config.sh port "$BSBM_CLASS")"
  DS_FILES="$DUMP_DOWNLOAD/bsbm.ttl"
  ( set -x ; rm -f "$DS_FILES" ) || exit 1
  if ! $DIR/scripts/bsbm.sh $FORCEIP4 \
       -g "$BSBM_DOWNLOAD" -o "$DS_FILES" "$DATASET_NAME"; then
    echo "Failed to generate BSBM dataset"
    exit 1
  fi
else
  PORT="$($DIR/config.sh port "$DATASET_NAME")"
  $DIR/scripts/lrb.sh $DUMPS_REDOWNLOAD_OPT -d "$DUMP_DOWNLOAD" -o "$DUMP_DOWNLOAD" "$DATASET_NAME"
  ( set -x ; test -d "$DATASET_NAME" ) || exit 1
  DS_FILES=""
  for i in $(find "$DATASET_NAME" -type f); do
    echo "Found RDF file $i"
    DS_FILES="$DS_FILES \"$i\""
  done
fi
if [ "$?" != 0 ]; then
  echo "Failed to get configured port for dataset $DATASET_NAME"; exit 1
fi

# Stop running Fuseki before rebuilding TDB
if [ ! -z "$PID_FILE" ]; then
  test ! -f "$PID_FILE" || kill_pid $(cat "$PID_FILE") fuseki-server.jar 20 5
fi

# Build TDB
test "$TDB_RELOAD" != 1 || sleep_rm "$TDB_LOC"
if [ ! -d "$TDB_LOC" ]; then
  ( set -x ; mkdir -p "$TDB_LOC" ) || exit 1
  bash -c "set -x ; \"$TDB_LOADER\" --loc \"$TDB_LOC\" $DS_FILES" || exit 1
fi

# Stop previous server (if any)
test ! -z "$PID_FILE" -a -f "$PID_FILE" && \
  kill_pid $(cat "$PID_FILE") fuseki-server.jar 20 5

#Start server
if [ -z "$PID_FILE" ]; then
  JVM_ARGS="-Xmx$FUSEKI_HEAP" "$FUSEKI_SERVER" "$TDB_VER_FLAG" --loc "$TDB_LOC" --port "$PORT" "/$FUSEKI_DS_NAME"
else
  JVM_ARGS="-Xmx$FUSEKI_HEAP" "$FUSEKI_SERVER" "$TDB_VER_FLAG" --loc "$TDB_LOC" --port "$PORT" "/$FUSEKI_DS_NAME" &
  PID=$!
  echo $PID > "$PID_FILE"
  disown $PID
  exit 0
fi

