#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/../scripts/lib.sh"
BSBM=
PID_FILE=/home/arch/virtuoso.pid
R_DIR=/home/arch/LargeRDFBench-exec

usage() {
  echo -e "Usage: $0 [-h] COMMAND ...\n"
  echo    "Options:"
  echo    "    -h Show this help message\n"
  echo    "    -b N_PRODUCTS"
  echo    "       Provision BSBM datasets instead of LRB datasets. Generate "
  echo    "       data for the given number of bsbm:Product instances. This "
  echo    "       is forwarded to the -pc option in the BSBM generator."
  echo    "Commands:"
  echo    "    provision Provision all dataset VMs. Install packages and shutdown."
  echo    "    virtuoso  Start virtuoso in all dataset VMs (and creates them, "
  echo    "              if needed). $(realpath ../eps) will be updated."
  echo    "    shutdown  Shutdown all dataset VMs"
  echo    "    terminate Shutdown and **TERMINATE** all dataset VMs"
}

while getopts "hb:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    b)
      BSBM=$(echo $OPTARG)
      check_opt -b '^[0-9]+$' "$BSBM"
      PID_FILE=/home/arch/fuseki.pid
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

function list_ds() {
  if [ -z "$BSBM" ]; then
    "$DIR/../config.sh" list_ds
  else
    "$DIR/../config.sh" list_bsbm_ds
  fi
}

function sparql_url() {
  ds=$1
  IP="$(cat "$DIR/vms/$ds.ip")"
  PORT=$($DIR/../config.sh port $ds)
  QUERY_PATH=$(test -z "$BSBM" && echo sparql || echo "$ds")
  echo "http://$IP:$PORT/$QUERY_PATH"
}

if [ "$#" == 0 ]; then
  echo "No command given" 1>&2
  usage 1>&2
  exit 1
fi

# Check variables
if [ -z "$AWS_KEYNAME" -o -z "$AWS_KEYPATH" -o -z "$AWS_REGION" ]; then
  echo "Missing envinronment variables! The following are required:"
  echo AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID"
  echo AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY"
  echo AWS_KEYNAME="$AWS_KEYNAME"
  echo AWS_KEYPATH="$AWS_KEYPATH"
  echo AWS_REGION="$AWS_REGION"
  exit 1
fi

if [ ! -f "$AWS_KEYPATH" ]; then
  echo "AWS_KEYPATH=$AWS_KEYPATH does not exist"
  exit 1
fi

function do_ssh() {
  ( set -x ; ssh -i "$DIR/../secrets/key.pem" -o StrictHostKeyChecking=no "$@" )
}

function instance_state () {
  ID=$1
  aws ec2 describe-instances --region "$AWS_REGION" \
      --filters "Name=instance-id,Values=$ID" \
      | sed -nE 's/^.*"Name" *: *"(.*)",? *$/\1/p'
}

function fetch_ip () {
  DS=$1
  ID=$(cat "$DIR/vms/$ds.id")
  while true; do
    STATE=$(instance_state $ID)
    if [ "$STATE" == "running" ]; then
      break;
    elif ( echo "$STATE" | grep "ing" &>/dev/null ); then
      echo "Transitional state $STATE."
      ( set -x ; sleep 30s )
    elif [ "$STATE" == "stopped" ]; then
      ( set -x ; aws ec2 start-instances --region "$AWS_REGION" --instance-ids $ID \
          && sleep 40s )
    else
      echo "Invalid state '$STATE' for fetch_ips with instance $ID for dataset $ds"
      break;
    fi
  done
  IP=$(aws ec2 describe-instances --region "$AWS_REGION" \
           --filters "Name=instance-id,Values=$ID" \
         | sed -nE 's/.*"PublicIpAddress": *"(.*)",.*/\1/p')
  echo "Instance $ID at $IP"
  if [ -z "$IP" ]; then
    echo "Machine $ID for dataset $ds has no IP!"
    exit 1
  fi
  echo "$IP" > "$DIR/vms/$ds.ip"
  # wait until ssh works
  for attempt in $(seq 1 6); do
    if ( do_ssh arch@$IP 'echo ping' ); then
      break;
    else
      sleep 10s
    fi
  done
}

function fetch_ips () {
  # Get all IPs
  for ds in $(list_ds); do
    fetch_ip $ds
  done
}

function provision() {
  DS=$1
  # Create instances
  cd "$DIR"
  ( set -x mkdir -p "$DIR/vms" ) || exit 1
  if [ -f "$DIR/vms/$DS.json" ]; then
    echo "VM for $DS already exists!"
  else
    ( set -x ; aws ec2 run-instances --image-id ami-0ca616d15c06bfe4c \
                   --region "$AWS_REGION" --count 1 --instance-type t3a.xlarge \
                   --key-name "$AWS_KEYNAME" --security-group-ids "$AWS_SECGROUP" \
                   --block-device-mappings \
                   '[{"DeviceName":"/dev/sda1","Ebs":{"VolumeSize":30}}]' \
                   > "$DIR/vms/$DS.json" ) || exit 1
    ID=$(sed -nE 's/.*"InstanceId": *"(.*)",.*/\1/p' "$DIR/vms/$DS.json")
    echo "$ID" > "$DIR/vms/$DS.id"
    echo "Created machine $ID for $DS"
    echo "Sleeping 30s until the machine has an IP"
    sleep 30s
  fi

  fetch_ip $DS

  # Provision
  IP="$(cat "$DIR/vms/$DS.ip")"
  if [ ! -f "$DIR/vms/$DS.prov" ]; then
    echo "Provisioning $IP for $DS"
    do_ssh arch@$IP <<EOF
      sudo pacman -Syu --noconfirm
      sudo pacman -S --noconfirm --needed git unzip p7zip maven jdk11-openjdk openssl-1.0 wget curl dos2unix
      git clone --depth 1 https://bitbucket.org/alexishuf/largerdfbench-exec.git LargeRDFBench-exec
      sudo rm -fr /var/cache/pacman/pkg/*.pkg.tar.*
EOF
    if [ "$?" != 0 ]; then
      echo "Failed to provision"
      ID="$(cat "$DIR/vms/$DS.ip")"
      (set -x ; aws ec2 stop-instances --region "$AWS_REGION" --instance-ids $ID )
      exit 1
    else
      touch "$DIR/vms/$DS.prov"
    fi
  fi
}

function do_ds_provision () {
  ds=$1
  provision $ds
  IP="$(cat "$DIR/vms/$ds.ip")"
  echo "Shutting down $IP for $ds"
  do_ssh -- arch@$IP 'sudo shutdown -h now'
  echo "Waiting 40s until effectively shutdown"
  sleep 40s
}

function do_provision () {
  # Create instances
  cd "$DIR"
  ( set -x mkdir -p "$DIR/vms" ) || exit 1
  for ds in $(list_ds); do
    if [ ! -f "$DIR/vms/$ds.prov" ]; then
      do_ds_provision $ds &
      sleep 5s # be polite with API calls
    fi
  done
  wait # Blocks until all provisioning oeprations complete
  echo "Provisioning complete!"
  sleep 5s
}

function do_virtuoso () {
  # Ensure all machines are proviosined (created and packages installed)
  do_provision

  # Usually, all machines already exist but are stopped
  IDS=""
  for ds in $(list_ds); do
    if [ -f "$DIR/vms/$ds.id" ]; then
      IDS="$IDS $(cat "$DIR/vms/$ds.id")"
    fi
  done
  if [ -z "$IDS" ]; then
    echo "No VM ids found! major provisioning failure"
    exit 1
  fi

  echo "Starting all instances that already exist"
  ( set -x ; aws ec2 start-instances --region "$AWS_REGION" \
                 --instance-ids $IDS ) || exit 1
  echo "Sleep before trying to fetch allocated IPs"
  ( set -x ; sleep 30s )
  fetch_ips

  #Start all datasets in parallel
  echo "Starting virtuoso/fuseki in each VMs..."
  for ds in $(list_ds); do
    STORE=$(test -z $BSBM && echo virtuoso || echo fuseki)
    CMD="$R_DIR/run.sh virtuoso -p $PID_FILE -m 16G $ds 2>&1 | tee virtuoso.log"
    test -z "$BSBM" || \
      CMD="$R_DIR/run.sh fuseki -p $PID_FILE bsbm:$BSBM:$ds 2>&1 | tee bsbm.log"
    IP=$(cat "$DIR/vms/$ds.ip")
    echo "Staring $STORE for $ds at $IP"
    do_ssh -f -- arch@$IP "nohup $CMD" &>"$DIR/vms/$ds.log"
  done
  ( set -x ; sleep 10s )
  # Wait for PID_FILE on all datasets
  echo "Waiting for $PID_FILE on each of the VMs..."
  for ds in $(list_ds); do
    IP=$(cat "$DIR/vms/$ds.ip")
    do_ssh -- arch@$IP "$R_DIR/scripts/wait_pid_file.sh $PID_FILE"
    echo "Waiting for $ds at $IP... Check $DIR/vms/$ds.log"
  done

  # Generate eps file
  rm "$DIR/../eps"
  for ds in $(list_ds); do
    echo "$ds: $(sparql_url $ds)" >> "$DIR/../eps"
  done
  echo ---
  cat "$DIR/../eps"
  echo ---

  echo "Virtuoso is up on all instances. Additional safety sleep"
  ( set -x ; sleep 20s )
}

function list_ds_with_vm() {
  EXT=$1
  test ! -z "$EXT" || EXT=id
  for name in $(ls "$DIR/vms/"); do
    echo "$name" | grep -E "\.$EXT\$" &>/dev/null || continue
    echo $name | sed -E "s@^(.+)\.$EXT@\1@g"
  done
}

function do_shutdown () {
  for ds in $(list_ds_with_vm ip); do
    IP="$(cat "$DIR/vms/$ds.ip")"
    echo "Killing virtuoso for $ds at $IP"
    ( do_ssh -- arch@$IP bash -c 'kill $(cat virtuoso.pid)' ) &>/dev/null &
    ( do_ssh -- arch@$IP bash -c 'kill $(cat fuseki.pid)' ) &>/dev/null &
  done
  wait
  ( set -x ; sleep 5s )
  for ds in $(list_ds_with_vm ip); do
    IP="$(cat "$DIR/vms/$ds.ip")"
    echo "Shutting down $IP for $ds"
    do_ssh -- arch@$IP 'rm -f virtuoso.pid fuseki.pid ; sudo shutdown -h now' &
  done
  wait
  echo "Sleeping to avoid further operations until VMs complete shutting down"
  sleep 30s;
}

function do_terminate () {
  IDS=""
  for ds in $(list_ds_with_vm id); do
    ID="$(cat "$DIR/vms/$ds.id")"
    IP="$(cat "$DIR/vms/$ds.ip")"
    IDS="$IDS $ID"
    echo "Terminating instance $ID at IP $IP for $ds"
  done
  IDS=$(echo $IDS | sed -E 's/^,//')
  ( set -x ; aws ec2 terminate-instances --region "$AWS_REGION" --instance-ids $IDS )
  if [ "$?" == 0 ]; then
    for ds in $(list_ds_with_vm id); do
      rm "$DIR/vms/$ds".*
    done
  fi
  echo "Sleeping to avoid further operations until VMs complete terminating"
  sleep 15s;
}

# Run one of the commands
ALL_ARGS=( "$0" "$@" )
CMD="${ALL_ARGS[$OPTIND]}"
if ! ( echo "$CMD" | grep -E '(provision|virtuoso|shutdown|terminate)' &>/dev/null ) ; then
  echo "Unexpected command: $CMD"
  usage 1>&2
  exit 1
fi

"do_$CMD"
