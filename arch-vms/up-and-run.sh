#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR/.."

source scripts/lib.sh

usage() {
  echo -e "Usage: $0 [-U] [-S] [-f] [-R] [-r RUNS] [-q QUERY] [-g GROUP]\n"
  echo    "Options:"
  echo    "    -U  Do not bring virtuoso instances up (assume they are already up)"
  echo    "    -S  Do not shutdown virtuoso VMs after running the experiments"
  echo    "    -f  Also Run FedX before riefederator"
  echo    "    -R  Do not run riefederator"
  echo    "    -b N_PRODUCTS"
  echo    "       Instead of setting up LargeRDFBench datasets, setup BSBM datasets "
  echo    "       with the given value passed to the BSBM generator via -pc"
  echo    "    -r NUMBER"
  echo    "       Number of (measured) runs for each (query, engine) pair"
  echo    "    -q QUERY"
  echo    "       Run only the query QUERY instead of all queries in queries/"
  echo    "    -g GROUP"
  echo    "       Run only queries of the given GROUP (S, C or B)"
  echo    "    -t MINUTES"
  echo    "       Timeout in minutes for a single run of a query on a mediator."
  echo    "       This is forwarded to the -t MINUTES option in "
  echo    "       bench-riefederator.sh and bench-fedx.sh. Default: 10 (20 for B* queries)"
}

DO_VIRTUOSO_UP=y
DO_SHUTDOWN=y
DO_FEDX=n
DO_RIEF=y
BSBM=
N_RUNS=5
ONLY_QUERY=
ONLY_GROUP=
DEF_TOUT=10
B_TOUT=30
# Only C5 is slow, all other C* queries should run under a 10 min timeout
C_TOUT=40

while getopts "hUSfRb:r:q:g:c:t:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    U)
      DO_VIRTUOSO_UP=n
      ;;
    S)
      DO_SHUTDOWN=n
      ;;
    f)
      DO_FEDX=y
      ;;
    R)
      DO_RIEF=n
      ;;
    b)
      BSBM="$OPTARG"
      check_opt -b '^[0-9]+$' "$BSBM"
      ;;
    r)
      N_RUNS=$(echo $OPTARG)
      check_opt -r '^[0-9]+$' "$N_RUNS"
      ;;
    q)
      ONLY_QUERY="$OPTARG"
      check_opt -g '^([SBC][0-9][0-9]?)|(query[0-9][0-9]?)$' "$ONLY_QUERY"
      ;;
    g)
      ONLY_GROUP="$OPTARG"
      check_opt -g '^[SBC]$' "$ONLY_GROUP"
      ;;
    t)
      DEF_TOUT=$(echo $OPTARG)
      B_TOUT=$(echo $OPTARG)
      C_TOUT=$(echo $OPTARG)
      check_opt -t '^[0-9]+$' "$DEF_TOUT"
      ;;
    *)
      echo "Bad option -$1" 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

if [ "$DO_VIRTUOSO_UP" = y ]; then
  if [ -z "$BSBM" ]; then
    arch-vms/datasets.sh virtuoso || exit 1
  else
    arch-vms/datasets.sh -b $BSBM virtuoso || exit 1
  fi
fi
rm -fr results nobck
mkdir results

# Reset queries/ contents
Q_DIR=$(mktemp -d)
rm -fr queries
git checkout -- queries
cp -r queries/* "$Q_DIR"

# Warmup -- initialize cache and see if servers are working
rm queries/* ; cp "$Q_DIR/S3" queries/
./run.sh bench riefederator -q S3  2>&1 | tee results/up-and-run.init.rief.log
INIT_LOG_TMP="$(mktemp)"
mv results/up-and-run.init.rief.log "$INIT_LOG_TMP"
rm -fr results
mkdir results
mv "$INIT_LOG_TMP" results/up-and-run.init.rief.log

run_all_in_queries() {
  SUBSUFFIX=$1
  TOUT=$2
  if [ -z "$TOUT" ]; then
    TOUT=$DEF_T
  fi
  PREHEAT_RUNS=${3:-2}
  if [ "$DO_FEDX" = y ]; then
    ./run.sh bench fedx -r $N_RUNS -p $PREHEAT_RUNS -t $TOUT 2>&1 | tee results/up-and-run.${SUBSUFFIX}.fedx.log
  fi
  if [ "$DO_RIEF" = y ]; then
    ./run.sh bench riefederator -j -r $N_RUNS -p $PREHEAT_RUNS -t $TOUT 2>&1 | tee results/up-and-run.${SUBSUFFIX}.rief.log
  fi
}

if [ ! -z "$ONLY_QUERY" ]; then
  rm -f queries/*
  if echo $ONLY_QUERY | grep -E '^query' &>/dev/null ; then
    ( set -x ; cp "bsbm-queries/$ONLY_QUERY" queries/ ) || exit 1
  else
    ( set -x ; cp "$Q_DIR/$ONLY_QUERY" queries/ ) || exit 1
  fi
  run_all_in_queries "$ONLY_QUERY" $DEF_TOUT
elif [ ! -z "$ONLY_GROUP" ]; then
  rm -f queries/* ; cp "$Q_DIR/$ONLY_GROUP"*  queries/
  run_all_in_queries "$ONLY_GROUP" $DEF_TOUT
elif [ ! -z "$BSBM" ]; then
  # These are the simples (for riefederator)
  ( set -x ; rm -f queries/* ; cp bsbm-queries/*  queries/ ) || exit 1
  run_all_in_queries BSBM $DEF_TOUT
else
  # These are the simples (for riefederator)
  ( set -x ; rm -f queries/* ; cp "$Q_DIR"/S*  queries/ ) || exit 1
  run_all_in_queries S $DEF_TOUT

  # These queries are SLOW. B5 takes ~13m20s with all machines in the same AWS region
  ( set -x ; rm -f queries/* ; cp "$Q_DIR"/B*  queries/ ) || exit 1
  run_all_in_queries B $B_TOUT 1

  # C queries are slower than S but faster than B, except for C5
  ( set -x ; rm -f queries/* ; cp "$Q_DIR"/C*  queries/ ) || exit 1
  run_all_in_queries C $C_TOUT
fi

if [ "$DO_SHUTDOWN" = y ]; then
  arch-vms/datasets.sh shutdown
fi
