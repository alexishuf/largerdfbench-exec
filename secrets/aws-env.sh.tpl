export AWS_ACCESS_KEY_ID='your-key-id'
export AWS_SECRET_ACCESS_KEY='your-secret-key'
export AWS_KEYNAME='ssh-keypair-name'
export AWS_KEYPATH='/path/to/private/key.pem'
export AWS_REGION="us-east-1"
export AWS_SECGROUP="sg-xxxxxxxx"
PS1="(aws keys) $PS1"
