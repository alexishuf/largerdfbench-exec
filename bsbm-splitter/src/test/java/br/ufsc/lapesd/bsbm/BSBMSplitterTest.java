package br.ufsc.lapesd.bsbm;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.RDF;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static org.apache.jena.riot.RDFLanguages.filenameToLang;
import static org.testng.Assert.*;

public class BSBMSplitterTest {
    private File tmp;
    private File outDir;
    private File datasetTTL;

    private @Nonnull Model loadDataset() throws IOException {
        Model model = ModelFactory.createDefaultModel();
        try (InputStream in = getClass().getResourceAsStream("dataset.ttl")) {
            RDFDataMgr.read(model, in, Lang.TTL);
        }
        return model;
    }

    private @Nonnull Map<BSBMSplitter.BSBMClass, Model> split(@Nonnull Model model) {
        Map<BSBMSplitter.BSBMClass, Model> map = new HashMap<>();
        for (ResIterator sIt = model.listSubjects(); sIt.hasNext();) {
            Resource subj = sIt.next();
            BSBMSplitter.BSBMClass[] cls = {null};
            subj.listProperties(RDF.type).forEachRemaining(s -> {
                if (s.getObject().isURIResource()) {
                    String uri = s.getObject().asResource().getURI();
                    BSBMSplitter.BSBMClass parsed = BSBMSplitter.BSBMClass.fromURI(uri);
                    if (parsed != null) {
                        assert cls[0] == null; //a single class per subject
                        cls[0] = parsed;
                    }
                }
            });
            assert cls[0] != null;
            Model out = map.computeIfAbsent(cls[0], k -> ModelFactory.createDefaultModel());
            subj.listProperties().forEachRemaining(out::add);
        }
        return map;
    }

    private @Nonnull Map<BSBMSplitter.BSBMClass, Model> loadSplit() throws IOException {
        Map<BSBMSplitter.BSBMClass, Model> map = new HashMap<>();
        File[] files = outDir.listFiles();
        assertNotNull(files);
        for (File file : files) {
            String baseName = file.getName().replaceAll("\\.[^.]+$", "");
            BSBMSplitter.BSBMClass cls = BSBMSplitter.BSBMClass.valueOf(baseName);
            assertNotNull(cls);
            Model model = ModelFactory.createDefaultModel();
            try (InputStream in = new FileInputStream(file)) {
                RDFDataMgr.read(model, in, filenameToLang(file.getName(), RDFLanguages.TTL));
            }
            assertNull(map.put(cls, model));
        }
        return map;
    }

    @BeforeMethod
    public void setUp() throws IOException {
        tmp = Files.createTempDirectory("riefederator").toFile();
        outDir = new File(tmp, "out");
        assertTrue(outDir.mkdirs());
        datasetTTL = new File(tmp, "dataset.ttl");
        try (OutputStream out = new FileOutputStream(datasetTTL);
             InputStream in = getClass().getResourceAsStream("dataset.ttl")) {
            assertNotNull(in);
            IOUtils.copy(in, out);
        }
        assertTrue(datasetTTL.exists());
        assertTrue(datasetTTL.length() > 0);
    }

    @AfterMethod
    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(tmp);
        tmp = null;
        outDir = null;
    }

    @Test
    public void testFragmentAllClasses() throws Exception {
        BSBMSplitter.main(new String[]{"--out-dir", outDir.getAbsolutePath(),
                                       datasetTTL.getAbsolutePath()});
        Map<BSBMSplitter.BSBMClass, Model> actual = loadSplit();
        Map<BSBMSplitter.BSBMClass, Model> expected = split(loadDataset());
        assertEquals(actual.keySet(), expected.keySet());
        for (Map.Entry<BSBMSplitter.BSBMClass, Model> e : actual.entrySet())
            assertTrue(e.getValue().isIsomorphicWith(expected.get(e.getKey())));
    }

    @Test
    public void testFragmentSingleClass() throws Exception {
        BSBMSplitter.main(new String[]{
                "--only-class", "Product",
                "--only-out-name", "Product",
                "--out-dir", outDir.getAbsolutePath(),
                datasetTTL.getAbsolutePath()
        });
        Map<BSBMSplitter.BSBMClass, Model> actual = loadSplit();
        Map<BSBMSplitter.BSBMClass, Model> expected = split(loadDataset());
        expected.keySet().removeIf(c -> c != BSBMSplitter.BSBMClass.Product);
        assertEquals(expected.size(), 1);
        assertEquals(actual.keySet(), expected.keySet());
        for (Map.Entry<BSBMSplitter.BSBMClass, Model> e : actual.entrySet())
            assertTrue(e.getValue().isIsomorphicWith(expected.get(e.getKey())));
    }
}