#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CHECKOUT_DIR=${CHECKOUT_DIR:-$HOME/LargeRDFBench-exec}

usage() {
  echo -e "Usage: $0 [-h] [-c CHECKOUT_DIR] COMMAND COMMAND_ARGS...\n"
  echo    "Options:"
  echo    "    -h Show this help message\n"
  echo    "    -c CHECKOUT_DIR"
  echo -e "       Checkout the LargeRDFBench-exec repo to the given CHECKOUT_DIR\n"
  echo    "Commands:"
  echo    "    fuseki       Start a fuseki endpoint for a patched dataset dump"
  echo    "    virtuoso     Start a virtuoso from the packaged endpoints"
  echo    "    bench        Run LargeRDFBench"
}

while getopts "hc:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    c)
      CHECKOUT_DIR=${OPTARG}
      ;;
  esac
done

if ( ! ( sed --version | grep 'GNU sed' >/dev/null ) ); then
  echo "These scripts make extensive use of GNU sed features. Mac OS sed (and likely other implementations) will fail miserably. Use something like homebrew and put a GNU sed in your \$PATH"
  exit 1
fi

ALL_ARGS=( "$0" "$@" )
CMD="${ALL_ARGS[$OPTIND]}"
if [ -z "$CMD" ]; then
  usage 1>&2
  exit 1
fi
CMD_ARGS=()
for i in $(seq $(($OPTIND + 1)) ${#ALL_ARGS[*]}); do
  CMD_ARGS+=("${ALL_ARGS[$i]}")
done

if [ -d "$DIR/patch" -a -f "$DIR/config.sh" ]; then
  echo "$DIR already looks like a checkout of LargeRDFBench-exec. Will not clone"
else
  set -x
  git clone --depth 1 https://bitbucket.org/alexishuf/largerdfbench-exec.git "$CHECKOUT_DIR" \
    || exit 1
  set +x
  DIR="$CHECKOUT_DIR"
fi

echo "Executing command $CMD " ${CMD_ARGS[*]}
CMD_SCRIPT="$DIR/scripts/$(echo "$CMD" | sed -E 's/\.sh$//').sh"
"$CMD_SCRIPT" ${CMD_ARGS[*]}
